# ENTRAÎNEMENT SPHERO & DJI SPARK
## Exercice 1
```
Consigne : Effectuer une séquence précise avec le drone et durant cette séquence prendre deux photos.
```

### La séquence de mouvement du drone
(Vidéo complète en cliquant sur le GIF)

<a href="https://youtu.be/Utu1FV49Rl4">
    <img src="VideoDemo/gif_sequence_video.gif" alt="La séquence de mouvement du drone" width="400"/>
</a>
<br>
Le code de la séquence se trouve dans le fichier [Exercice1ViewController.swift](SparkPerso/SparkPerso/SparkStarter/EXERCICE1/Exercice1ViewController.swift) <br>
Des nouveaux mouvements ont été rajouté pour pouvoir contrôler la caméra du drone dans le fichier [BasicMove.swift](SparkPerso/SparkPerso/SparkStarter/TP1/BasicMove.swift) avec leurs implémentations dans le fichier [SparkMovementManager.swift](SparkPerso/SparkPerso/SparkStarter/TP1/SparkMovementManager.swift).

### L'interface de création de séquence 
(Vidéo complète en cliquant sur le GIF)

<a href="https://youtu.be/Rk3Yl-QfVro">
    <img src="VideoDemo/gif_ipad_video.gif" alt="L'interface de création de séquence" width="400"/>
</a>

<br>

## Exercice 2
```
Consigne : Faire un mouvement circulaire avec le Sphero.
```
<a href="https://youtu.be/CzFcmbvEsAQ">Lien de la vidéo</a>

Le code du cercle [Exercice2ViewController.swift](SparkPerso/SparkPerso/SparkStarter/EXERCICE 2/Exercice2ViewController.swift) <br>
