//
//  SocketIOManager.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 30/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation
import SocketIO

class SocketIOManager {
    struct Ctx {
        var ip:String
        var port:String
        var modeVerbose:Bool
        
        func fullIp() -> String {
            return "http://"+ip+":"+port
        }
        
        static func debugContext() -> Ctx {
            return Ctx(ip: "172.28.59.24", port: "3000", modeVerbose: false)
        }
    }
    
    static let instance = SocketIOManager()
    
    var manager:SocketManager? = nil
    var socket:SocketIOClient? = nil
    
    func setup(ctx:Ctx = Ctx.debugContext()) {
        manager = SocketManager(socketURL: URL(string: ctx.fullIp())!, config: [.log(ctx.modeVerbose), .compress])
        socket = manager?.defaultSocket
    }
    
    func connect(callback:@escaping ()->()) {
        listenToConnection(callback: callback)
        socket?.connect()
    }
    
    func listenToConnection(callback:@escaping ()->()) {
        socket?.on(clientEvent: .connect) {data, ack in
            print("socket connected")
            callback()
        }
    }
    
    func listenToChannel(channel:String, callback:@escaping (String?)->()) {
        socket?.on(channel) {data, ack in
            
            if let d = data.first,
                let dataStr = d as? String {
                callback(dataStr)
            }else{
                callback(nil)
            }
            
            
            ack.with("Got your currentAmount", "dude")
        }
    }
    
    func writeValue(_ value:String,toChannel channel:String, callback:@escaping ()->()) {
        socket?.emit(channel, value)
        
        
    }
    
    /*

    socket.on(clientEvent: .connect) {data, ack in
        print("socket connected")
    }

    socket.on("currentAmount") {data, ack in
        guard let cur = data[0] as? Double else { return }
        
        socket.emitWithAck("canUpdate", cur).timingOut(after: 0) {data in
            socket.emit("update", ["amount": cur + 2.50])
        }

        ack.with("Got your currentAmount", "dude")
    }

    socket.connect()*/
}
