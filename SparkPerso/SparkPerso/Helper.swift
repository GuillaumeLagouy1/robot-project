//
//  Helper.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 17/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation
import UIKit

func delay(_ delay:Float, closure:@escaping ()->()) {
    let when = DispatchTime.now() + Double(delay)
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

extension UIViewController {
    func askForDuration(callback:@escaping (Float)->()) {
        let ac = UIAlertController(title: "Enter answer", message: nil, preferredStyle: .alert)
        ac.addTextField{(txt) in
            txt.placeholder = "Duration"
        }
        
        let submitAction = UIAlertAction(title: "Submit", style: .default) { [unowned ac] _ in
            if let durationTxt = ac.textFields![1].text,
                let duration = Float(durationTxt){

                callback(duration)
            }
        }

        ac.addAction(submitAction)

        present(ac, animated: true)
    }
    
    func askForDurationAndSpeed(callback:@escaping (Float, Float)->()) {
        let ac = UIAlertController(title: "Enter answer", message: nil, preferredStyle: .alert)
        ac.addTextField{(txt) in
            txt.placeholder = "Speed"
        }
        ac.addTextField{(txt) in
            txt.placeholder = "Duration"
        }
        
        let submitAction = UIAlertAction(title: "Submit", style: .default) { [unowned ac] _ in
            if let speedTxt = ac.textFields![0].text,
                let durationTxt = ac.textFields![1].text,
                let speed = Float(speedTxt),
                let duration = Float(durationTxt){

                callback(speed, duration)
            }
        }

        ac.addAction(submitAction)

        present(ac, animated: true)
    }
    
    func askForDurationAndSpeedAndHeading(callback:@escaping (Float, Float, Float)->()) {
        let ac = UIAlertController(title: "Enter answer", message: nil, preferredStyle: .alert)
        ac.addTextField{(txt) in
            txt.placeholder = "Speed"
        }
        ac.addTextField{(txt) in
            txt.placeholder = "Duration"
        }
        ac.addTextField{(txt) in
            txt.placeholder = "Heading"
        }
        
        let submitAction = UIAlertAction(title: "Submit", style: .default) { [unowned ac] _ in
            if let speedTxt = ac.textFields![0].text,
                let durationTxt = ac.textFields![1].text,
                let headingTxt = ac.textFields![2].text,
                let speed = Float(speedTxt),
                let duration = Float(durationTxt),
                let heading = Float(headingTxt){

                callback(speed, duration, heading)
            }
        }

        ac.addAction(submitAction)

        present(ac, animated: true)
    }
}
