//
//  Exercice2ViewController.swift
//  SparkPerso
//
//  Created by Pierre Brelaud on 15/11/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit

class Exercice2ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        SharedToyBox.instance.bolt?.setBackLed(color: .red)
    }
    
    var currentHeading:Double = 0 {
        didSet{ }
    }
    
    //increase heading value
    var increase:Double = 90/4.68
    //temp heading used in rotate
    var tempHeading:Double = 0
    //sphero speed
    var speed:Double = 250
    
    
    @IBAction func doCircleClicked(_ sender: Any) {
        moveRotateRight()
        
        delay(8) {
            self.moveRotateRight()
            delay(8) {
                self.moveRotateRight()
                delay(8) {
                    self.moveRotateRight()
                }
            }
        }
    }
    
    //-----------  MOVEMENT
    
    func moveRotateRight() {
        tempHeading = currentHeading + 90
        
        delay(2) {
            self.do90right()
            //45
            delay(1.5) {
                self.currentHeading += self.increase
                self.rotate(duration: 0.26)
                
                //90
                delay(1.5) {
                    self.currentHeading += (self.increase + 16)
                    self.rotate(duration: 0.26)
                    
                    delay(2) {
                        self.updateHeading(heading: self.tempHeading)
                    }
                }
            }
        }
    }
    
    func rotate() {
        tempHeading = currentHeading + 90
        
        delay(2) {
            self.do90right()
            delay(1.5) {
                self.currentHeading += self.increase
                self.rotate(duration: 0.26)
                
                delay(1.5) {
                    self.currentHeading += (self.increase + 16)
                    self.rotate(duration: 0.26)
                    
                    delay(2) {
                        self.updateHeading(heading: self.tempHeading)
                    }
                }
            }
        }
    }
    
    func do90right() {
        currentHeading += 90
        updateHeading(heading: currentHeading)
    }
    
    func forward(duration:Float) {
        SharedToyBox.instance.bolt?.roll(heading: currentHeading, speed: speed)
        delay(duration) {
            self.stop()
        }
    }
    
    func updateHeading(heading:Double) {
        currentHeading = heading
        SharedToyBox.instance.bolt?.roll(heading: currentHeading, speed: 0)
    }
    
    func rotate(duration:Float) {
        SharedToyBox.instance.bolt?.roll(heading: currentHeading, speed: speed)
        delay(duration) {
            self.stop()
            self.currentHeading += self.increase
            self.updateHeading(heading: self.currentHeading)
        }
    }
    
    func stop() {
        SharedToyBox.instance.bolt?.stopRoll(heading: currentHeading)
    }
    
    //----------- MOVEMENT END
    
    
    //----------- HEADING SLIDER
    @IBAction func headingUpdatedSlider(_ sender: UISlider) {
        currentHeading = Double(sender.value).rounded()
        SharedToyBox.instance.bolt?.rotateAim(currentHeading)
    }
    //----------- HEADING SLIDER END
    

}
