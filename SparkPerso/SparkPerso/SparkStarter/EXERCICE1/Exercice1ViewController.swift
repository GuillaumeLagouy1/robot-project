//
//  Exercice1ViewController.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 09/11/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit
import DJISDK
import VideoPreviewer

class Exercice1ViewController: UIViewController {
    
    @IBOutlet weak var sequenceTextView: UITextView!
    @IBOutlet weak var videoPreviewView: UIView!
    @IBOutlet weak var firstPictureImageView: UIImageView!
    @IBOutlet weak var secondPictureImageView: UIImageView!
    
    let prev = VideoPreviewer()
    var sparkMovementManager:SparkMovementManager? = nil
    
    var sequence = [BasicMove](){
        didSet{
            DispatchQueue.main.async{
                self.displaySequenceOnTextView()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* SEQUENCE Exo 1 */
        
        //Up and rotate 180° 1m
        self.sequence.append(UpAndRotate(durationInSec: 3, speed: 0.3))
        //Put camera under take first picture and put camera up
        self.sequence.append(LookUnder(duration: 1.4))
        self.sequence.append(BasicMove(durationInSec: 0.5, direction: .wait, speed: 0))
        self.sequence.append(TakePicture(previewer: prev!, imageView: firstPictureImageView, durationInSec: 1))
        self.sequence.append(LookFront(duration: 1.4))
        //Backward 3m
        self.sequence.append(Back(durationInSec: 4.5, speed: 0.3))
        //Wait
        self.sequence.append(BasicMove(durationInSec: 1, direction: .wait, speed: 0))
        //Down 1m
        self.sequence.append(Down(durationInSec: 1.7, speed: 0.7))
        //Front 3m
        self.sequence.append(Front(durationInSec: 4, speed: 0.3))
        self.sequence.append(BasicMove(durationInSec: 1, direction: .wait, speed: 0))
        //Up 1m
        self.sequence.append(Up(durationInSec: 2.7, speed: 0.3))
        //Put camera under take second picture and put camera up
        self.sequence.append(LookUnder(duration: 1.4))
        self.sequence.append(BasicMove(durationInSec: 0.5, direction: .wait, speed: 0))
        self.sequence.append(TakePicture(previewer: prev!, imageView: secondPictureImageView, durationInSec: 1))
        self.sequence.append(LookFront(duration: 1.4))
        //Landing
        
    }
    
    @IBAction func onUpTouched(_ sender: Any) {
        self.askForDurationAndSpeed{(speed, duration) in
            self.sequence.append(Up(durationInSec: duration, speed: speed))
        }
    }
    
    @IBAction func onDownTouched(_ sender: Any) {
        self.askForDurationAndSpeed{(speed, duration) in
            self.sequence.append(Down(durationInSec: duration, speed: speed))
        }
    }
    
    @IBAction func onFrontTouched(_ sender: Any) {
        self.askForDurationAndSpeed{(speed, duration) in
            self.sequence.append(Front(durationInSec: duration, speed: speed))
        }
    }
    
    @IBAction func onBackTouched(_ sender: Any) {
        self.askForDurationAndSpeed{(speed, duration) in
            self.sequence.append(Back(durationInSec: duration, speed: speed))
        }
    }
    
    @IBAction func onUpAndRotateTouched(_ sender: Any) {
        self.sequence.append(UpAndRotate(durationInSec: 3, speed: 0.3))
        
    }
    
    @IBAction func onWaitTouched(_ sender: Any) {
        self.askForDuration{(duration) in
            self.sequence.append(BasicMove(durationInSec: duration, direction: .wait, speed: 0))
        }
    }
    
    @IBAction func onTakePictureTouched(_ sender: Any) {
        if (firstPictureImageView.image == nil){
            self.sequence.append(TakePicture(previewer: prev!, imageView: firstPictureImageView, durationInSec: 1))
        } else {
            self.sequence.append(TakePicture(previewer: prev!, imageView: secondPictureImageView, durationInSec: 1))
        }
        
    }
    
    @IBAction func onClearTouched(_ sender: Any) {
        sequence = []
        sequenceTextView.text = ""
        sparkMovementManager?.clearSequence()
    }
    
    @IBAction func onStartClicked(_ sender: Any) {
        print("Start")
        sparkMovementManager?.playSequence()
    }
    
    @IBAction func onStopClicked(_ sender: Any) {
        print("Stop")
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            mySpark.mobileRemoteController?.leftStickVertical = 0.0
            mySpark.mobileRemoteController?.leftStickHorizontal = 0.0
            mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
            mySpark.mobileRemoteController?.rightStickVertical = 0.0
        }
        sparkMovementManager?.redAlert()
    }
    
    func displaySequenceOnTextView(){
        sparkMovementManager = SparkMovementManager(sequence: sequence)
        if let desc = sparkMovementManager?.sequenceDescription(){
            sequenceTextView.text = desc
        }
    }
    
    /*  ----------------------- CAMERA ----------------------- */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let _ = DJISDKManager.product() {
            if let camera = self.getCamera(){
                camera.delegate = self
                self.setupVideoPreview()
            }
           
            GimbalManager.shared.setup(withDuration: 1.0, defaultPitch: -28.0)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let camera = self.getCamera() {
            camera.delegate = nil
        }
        self.resetVideoPreview()
    }
    
    func getCamera() -> DJICamera? {
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
             return mySpark.camera
        }
       
        return nil
    }
    
    func setupVideoPreview() {
        prev?.setView(self.videoPreviewView)
        
        if let _ = DJISDKManager.product(){
            let video = DJISDKManager.videoFeeder()
           
            DJISDKManager.videoFeeder()?.primaryVideoFeed.add(self, with: nil)
        }
        
        prev?.start()
    }
    
    func resetVideoPreview() {
        prev?.unSetView()
        DJISDKManager.videoFeeder()?.primaryVideoFeed.remove(self)
    }
    /*  ------------------------------------------------------ */
}

extension Exercice1ViewController:DJIVideoFeedListener {
    func videoFeed(_ videoFeed: DJIVideoFeed, didUpdateVideoData videoData: Data) {
        videoData.withUnsafeBytes { (bytes:UnsafePointer<UInt8>) in
            prev?.push(UnsafeMutablePointer(mutating: bytes), length: Int32(videoData.count))
        }
    }
}

extension Exercice1ViewController:DJICameraDelegate {
   
}
