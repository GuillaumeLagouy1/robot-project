//
//  BasicMove.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 18/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation
import VideoPreviewer

class BasicMove{
    var durationInSec:Float
    var speed:Float
    var direction:Direction
    var heading:Double = 0.0
    var previewer:VideoPreviewer?
    var imageView:UIImageView?
    
    enum Direction{
        case front,back,rotateLeft,
        rotateRight,up,down,
        translateLeft, translateRight, stop,
        none, wait, upAndRotate
    }
    var description:String{
        get{
            return "Move \(direction) during \(durationInSec)s with \(speed) speed"
        }
    }
    
    init(durationInSec:Float, direction:Direction, speed:Float) {
        self.durationInSec  = durationInSec
        self.direction      = direction
        self.speed          = speed
    }
}





class Front:BasicMove{
    init(durationInSec:Float, speed:Float) {
        super.init(durationInSec: durationInSec, direction: .front, speed: speed)
    }
}

class Up:BasicMove{
    init(durationInSec:Float, speed:Float) {
        super.init(durationInSec: durationInSec, direction: .up, speed: speed)
    }
}

class Down:BasicMove{
    init(durationInSec:Float, speed:Float) {
        super.init(durationInSec: durationInSec, direction: .down, speed: speed)
    }
}

class Back:BasicMove{
    init(durationInSec:Float, speed:Float) {
        super.init(durationInSec: durationInSec, direction: .back, speed: speed)
    }
}

class UpAndRotate:BasicMove{
    init(durationInSec:Float, speed:Float) {
        super.init(durationInSec: durationInSec, direction: .upAndRotate, speed: speed)
    }
}

class RotateRight90:BasicMove{
    let duration:Float = 4.5
    init() {
        super.init(durationInSec: duration, direction: .rotateRight, speed: 0.5)
    }
}

class RotateLeft90:BasicMove{
    let duration:Float = 4.5
    init() {
        super.init(durationInSec: duration, direction: .rotateLeft, speed: 0.5)
    }
}

class RotateRight:BasicMove{
    init(duration:Float, speed:Float) {
        super.init(durationInSec: duration, direction: .rotateRight, speed: speed)
    }
}

class RotateLeft:BasicMove{
    init(duration:Float, speed:Float) {
        super.init(durationInSec: duration, direction: .rotateLeft, speed: speed)
    }
}

class Stop:BasicMove{
    init() {
        super.init(durationInSec: 0.0, direction: .stop, speed: 0)
    }
}

class SpheroMove:BasicMove{
    init(heading:Double, durationInSec: Float, speed: Float) {
        super.init(durationInSec: durationInSec, direction: .front, speed: speed)
        self.heading = heading
    }
}

class TakePicture:BasicMove{
    init(previewer:VideoPreviewer, imageView:UIImageView, durationInSec:Float) {
        super.init(durationInSec: durationInSec, direction: .none, speed: 0)
        self.previewer = previewer
        self.imageView = imageView
    }
}

class LookUnder:BasicMove{
    init(duration:Float){
        super.init(durationInSec: duration, direction: .none, speed: 0)
    }
}

class LookFront:BasicMove{
    init(duration:Float){
        super.init(durationInSec: duration, direction: .none, speed: 0)
    }
}

