//
//  SparkMovementManager.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 18/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation
import DJISDK
import VideoPreviewer

class SparkMovementManager:GenericMovementManager {
    override init(sequence: [BasicMove]) {
        super.init(sequence: sequence)
        self.sequence.append(Stop())
    }
    
    override func playMove(move: BasicMove, moveDidFinish: @escaping (() -> ())) {
        talkWithSDK(move: move)
        print(move.description)
        delay(move.durationInSec){
            moveDidFinish()
        }
    }
    
    func talkWithSDK(move:BasicMove) {
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            mySpark.mobileRemoteController?.leftStickVertical = 0.0
            mySpark.mobileRemoteController?.leftStickHorizontal = 0.0
            mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
            mySpark.mobileRemoteController?.rightStickVertical = 0.0
            
            let speed = move.speed
            let previewer = move.previewer
            let imageView = move.imageView
            
            switch move {
            case is Front:
                mySpark.mobileRemoteController?.rightStickVertical = speed
            case is Up:
                mySpark.mobileRemoteController?.leftStickVertical = speed
            case is Down:
                mySpark.mobileRemoteController?.leftStickVertical = -speed
            case is Back:
                mySpark.mobileRemoteController?.rightStickVertical = -speed
            case is TakePicture:
                takePictureAction(previewer: previewer!, imageView: imageView!)
            case is UpAndRotate:
                mySpark.mobileRemoteController?.leftStickVertical = speed
                mySpark.mobileRemoteController?.leftStickHorizontal = 0.8
            case is LookFront:
                lookFront()
            case is LookUnder:
                lookUnder()
            case is RotateRight:
                mySpark.mobileRemoteController?.leftStickHorizontal = speed
            case is RotateLeft:
                mySpark.mobileRemoteController?.leftStickHorizontal = -speed
            case is RotateRight90:
                mySpark.mobileRemoteController?.leftStickHorizontal = speed
            case is RotateLeft90:
                mySpark.mobileRemoteController?.leftStickHorizontal = -speed
            case is Stop:
                mySpark.mobileRemoteController?.leftStickVertical = 0.0
                mySpark.mobileRemoteController?.leftStickHorizontal = 0.0
                mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
                mySpark.mobileRemoteController?.rightStickVertical = 0.0
            default:
                break
            }
        }
    }
    
    func takePictureAction(previewer:VideoPreviewer, imageView:UIImageView) {
        previewer.snapshotThumnnail{ (image) in
            if let img = image {
             imageView.image = img
            }
        }
    }
    
    func lookUnder() {
        GimbalManager.shared.lookUnder()
    }
    
    func lookFront() {
        GimbalManager.shared.lookFront()
    }
}
