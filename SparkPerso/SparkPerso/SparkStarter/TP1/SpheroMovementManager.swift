//
//  SpheroMovementManager.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 18/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation

class SpheroMovementManager:GenericMovementManager {
    override func playMove(move: BasicMove, moveDidFinish: @escaping (() -> ())) {
        talkWithSDK(move: move)
        delay(move.durationInSec){
            moveDidFinish()
        }
    }
    
    func talkWithSDK(move:BasicMove) {
        print("Move from Sphero SDK")
        
        SharedToyBox.instance.bolt?.roll(heading: move.heading, speed: Double(move.speed))
    }
}
