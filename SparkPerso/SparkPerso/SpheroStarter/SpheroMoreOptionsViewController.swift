//
//  SpheroMoreOptionsViewController.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 14/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation
import UIKit

class SpheroMoreOptionsViewController: UIViewController {
    
    @IBOutlet weak var blueBtn: UIButton!
    @IBOutlet weak var redBtn: UIButton!
    @IBOutlet weak var greenBtn: UIButton!
    
    private let CORNER_RADIUS = CGFloat(8)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        blueBtn.layer.cornerRadius = CORNER_RADIUS
        redBtn.layer.cornerRadius = CORNER_RADIUS
        greenBtn.layer.cornerRadius = CORNER_RADIUS
    }
    
    @IBAction func onColorBtnTouched(_ sender: UIButton) {
        //b.drawMatrix(fillFrom: Pixel(x: 0, y: 0), to: Pixel(x: 59, y: 59), color: .green)
        
        SharedToyBox.instance.bolts.map{
            $0.drawMatrix(fillFrom: Pixel(x: 0, y: 0), to: Pixel(x: 59, y: 59), color: sender.backgroundColor! )
        }
    }
    
}
