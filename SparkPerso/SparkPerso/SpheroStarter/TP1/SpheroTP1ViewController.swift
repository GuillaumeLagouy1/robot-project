//
//  SpheroTP1ViewController.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 18/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit

class SpheroTP1ViewController: UIViewController {

    var spheroMovementManager:SpheroMovementManager? = nil
    var currentHeading = 0.0
    
    @IBOutlet weak var sequenceTextView: UITextView!
    
    var sequence = [BasicMove](){
        didSet{
            DispatchQueue.main.async{
                self.displaySequenceOnTextView()
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*let seq = [Front(durationInSec: 0.5),
                   RotateRight90(),
                   Front(durationInSec: 0.5)]
        */
        //sparkMovementManager =  SparkMovementManager(sequence: sequence)
        
        SocketIOManager.instance.setup()
        SocketIOManager.instance.connect {
            print("Connection!")
            SocketIOManager.instance.writeValue("Toto", toChannel: "test") {
                
            }
            SocketIOManager.instance.listenToChannel(channel: "test") { receivedStr in
                if let str = receivedStr {
                    self.manageCommand(str: str)
                    
                } else{
                    print("Received wrong type")
                }
            
            }
        }
    }
    
    func manageCommand(str:String) {
        switch str {
        case let t where t.contains("FRONT"):
            let components = t.split(separator: ":")
            let parameters = Array(components.dropFirst())
            if let d = Float(parameters[0]),
                let s = Float(parameters[1]){
                self.sequence.append(SpheroMove(heading: self.currentHeading, durationInSec: d, speed: s))
            }
            
        case "LEFT":
            self.sequence.append(SpheroMove(heading: self.currentHeading - 90.0, durationInSec: 0.0, speed: 0.0))
            self.currentHeading -= 90.0
        case "RIGHT":
            self.sequence.append(SpheroMove(heading: self.currentHeading + 90.0, durationInSec: 0.0, speed: 0.0))
            self.currentHeading += 90.0
        case "STOP": spheroMovementManager?.redAlert()
        default:break
        }
    }
    
    @IBAction func stopButtonClicked(_ sender: Any) {
        print("Stop")
        spheroMovementManager?.redAlert()
    }
    
    @IBAction func startButtonClicked(_ sender: Any) {
        print("Start")

        spheroMovementManager?.playSequence()
    }
    
    
    
    @IBAction func rotateLeftClicked(_ sender: Any) {
        self.sequence.append(SpheroMove(heading: self.currentHeading - 90.0, durationInSec: 1.0, speed: 0.0))
        self.currentHeading -= 90.0
    }
    
    @IBAction func frontClicked(_ sender: Any) {
        self.sequence.append(SpheroMove(heading: self.currentHeading, durationInSec: 2.0, speed: 25.0))
    }
    
    @IBAction func rotateRightClicked(_ sender: Any) {
        print(self.currentHeading)
        self.sequence.append(SpheroMove(heading: self.currentHeading + 90.0, durationInSec: 1.0, speed: 0.0))
        self.currentHeading += 90.0
    }
    
    @IBAction func displaySequenceClicked(_ sender: Any) {
        displaySequenceOnTextView()
        
    }
    
    @IBAction func clearSequenceClicked(_ sender: Any) {
        sequence = []
        sequenceTextView.text = ""
        spheroMovementManager?.clearSequence()
    }
    
    func displaySequenceOnTextView(){
        spheroMovementManager = SpheroMovementManager(sequence: sequence)
        if let desc = spheroMovementManager?.sequenceDescription(){
            sequenceTextView.text = desc
        }
    }
    
    @IBAction func onCircleTouched(_ sender: Any) {
        doCircle();
    }
    
    var speed:Double = 100
    var delay:UInt32 = 250000
    var rotateValue:Double = 25.0
    var circleMouvementCount:Double = 1
    var aim:Double = 0.0
    
    func doCircle() {

        //self.sequence.append(Front(durationInSec: 10))
        
        //super.init(durationInSec: durationInSec, direction: .front, speed: 0.5)
        
        let count:Int = Int(15 * circleMouvementCount)
        
        for n in 1...(count) {
            //movement
            SharedToyBox.instance.bolt?.roll(heading: self.aim, speed: self.speed)
            //increment aim
            self.aim = self.aim + rotateValue
            //stop before last move
            if(n == count) {
                SharedToyBox.instance.bolt?.stopRoll(heading: aim)
            }
            //delay
            usleep(self.delay)
        }
    }

}
