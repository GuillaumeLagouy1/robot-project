//
//  VoiceRecognitionViewController.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 02/11/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation
import UIKit
import Speech

class VoiceRecognitionViewController: UIViewController, SFSpeechRecognizerDelegate {
    
    @IBOutlet weak var detectedTextLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var canvas: UIImageView!
    
    let audioEngine = AVAudioEngine()
    let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "fr"))!
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    let shapes = ["cercle", "carré", "triangle"]
    let colors = [
        Color(frenchName: "rouge", uiColor: .red),
        Color(frenchName: "vert", uiColor: .green),
        Color(frenchName: "bleu", uiColor: .blue)
    ]
    
    class Color {
        let frenchName:  String
        let uiColor:  UIColor
        
        init(frenchName: String, uiColor: UIColor){
            self.frenchName = frenchName
            self.uiColor = uiColor
        }
    }
    
    
    
    var isRecording = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func recordAndRecognizeSpeech() {
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            return print(error)
        }
        
        guard let myRecognizer = SFSpeechRecognizer() else {
            // A recognizer is not supported for the current locale
            return
        }
        if !myRecognizer.isAvailable {
            // A recognizer is not available right now
            return
        }
        
        recognitionTask = speechRecognizer.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {
                
                let bestString = result.bestTranscription.formattedString
                self.detectedTextLabel.text = bestString
                
                var hasShape: Bool = false
                var hasColor: Bool = false
                var lastShape: String = ""
                var lastColor: UIColor = UIColor()
                
                for segment in result.bestTranscription.segments {
                    
                    // detect shapes in speech
                    if (self.shapes.contains(segment.substring.lowercased())){
                        hasShape = true;
                        lastShape = segment.substring.lowercased()
                    }
                    
                    // detect colors in speech
                    for color in self.colors {
                        if color.frenchName == segment.substring.lowercased(){
                            hasColor = true
                            lastColor = color.uiColor
                        }
                    }
                    
                    if(hasShape && hasColor ){
                        self.stopRecording()
                        self.drawShape(shape: lastShape, color: lastColor)
                    }
                }
                
            } else if let error = error {
                print(error)
            }
        })
    }
    
    func stopRecording() {
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
        request.endAudio()
        // Cancel the previous task if it's running
        if let recognitionTask = recognitionTask {
            recognitionTask.finish()
            self.recognitionTask = nil
        }
        
        self.startButton.setTitle("Start recording", for: .normal)
        self.startButton.backgroundColor = .systemGreen
        self.isRecording = false
    }
    
    @IBAction func startButtonTapped(_ sender: Any) {
        self.clearCanvas()
        self.detectedTextLabel.text = ""
        if(isRecording){
            
            stopRecording()
            
        
        } else {
            self.recordAndRecognizeSpeech()
            
            self.startButton.setTitle("Stop recording", for: .normal)
            self.startButton.backgroundColor = .red
            self.isRecording = true
        }
    }
    
    func drawShape(shape: String, color: UIColor) {
        let canvasX = canvas.frame.origin.x
        let canvasY = canvas.frame.origin.y
        let canvasW = canvas.frame.size.width
        let canvasH = canvas.frame.size.height
        
        switch shape {
        case "cercle":
            drawCircle(color: color.cgColor, x: Int(canvasX + canvasW / 2), y: Int(canvasY + canvasH / 2))
        case "carré":
            drawSquare(color: color.cgColor, x: Int(canvasX + canvasW / 2), y: Int(canvasY + canvasH / 2))
        default: break
        }
    }
    
    func drawCircle(color: CGColor, x: Int, y: Int) {
        let layer = CAShapeLayer()
        
        layer.path = UIBezierPath(arcCenter: CGPoint(x: x,y: y), radius: CGFloat(100), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true).cgPath
        layer.fillColor = color
        
        view.layer.addSublayer(layer)
        
    }
    
    func drawSquare(color: CGColor, x: Int, y: Int) {
        let layer = CAShapeLayer()
        
        layer.path = UIBezierPath(rect: CGRect(x: x - 50, y: y - 50, width: 100, height: 100)).cgPath
        layer.fillColor = color
        
        view.layer.addSublayer(layer)
    }
    
    func clearCanvas() {
        let canvasX = canvas.frame.origin.x
        let canvasY = canvas.frame.origin.y
        let canvasW = canvas.frame.size.width
        let canvasH = canvas.frame.size.height
        
        let layer = CAShapeLayer()
        
        layer.path = UIBezierPath(rect: CGRect(x: canvasX, y: canvasY, width: canvasW, height: canvasH)).cgPath
        layer.fillColor = UIColor.lightGray.cgColor
        
        view.layer.addSublayer(layer)
    }
}
